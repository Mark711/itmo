from ops import div

try:
	a = int(input())
	b = int(input())
	result = div(a, b)
except (ArithmeticError, ValueError) as exception: 
	print(exception)
	print(type(exception))
	print(dir(exception))

print(result)
