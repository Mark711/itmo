class MyZeroDivisionError(ZeroDivisionError):
	def __init__(self, number):
		text = f"division {number} by zero"
		ZeroDivisionError.__init__(self, text, number)


def div(a,b):
	if b ==0:
		raise MyZeroDivisionError(a)
	return a / b
